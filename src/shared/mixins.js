import { format, addDays } from 'date-fns';

export default {
  filters: {
    date: (value) => format(value, 'DD.MM.YYYY') 
  },
  methods: {
    getEmptyTask: () => ({
      name: null,
      description: null,
      estimatedDate: addDays(new Date(), 1),
      editable: false,
    })
  }
}