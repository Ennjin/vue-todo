import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const ADD_TASK = '[task] ADD_TASK';
export const DELETE_TASK = '[task] DELETE_TASK';
export const SET_COMPLETE = '[task] SET_COMPLETE';
export const SET_EDITABLE = '[task] SET_EDITABLE';
export const UPDATE_TASK = '[task] UPDATE_TASK';

export const TASK_LIST_GETTER = '[task] GET_TASK_LIST';


export default new Vuex.Store({
  state: {
    taskList: []
  },
  mutations: {
    [ADD_TASK](state, payload) {
      state.taskList.push({
        id: Math.random().toString().substring(2, 7),
        createdDate: new Date().setHours(0, 0, 0, 0),
        ...payload
      });
    },
    [DELETE_TASK](state, payload) {
      state.taskList = state.taskList.filter(elem => elem.id !== payload);
    },
    [SET_COMPLETE](state, payload) {
      Vue.set(state.taskList, payload, {
        ...state.taskList[payload],
        completeDate: new Date().setHours(0, 0, 0, 0)
      });
    },
    [SET_EDITABLE](state, payload) {
      Vue.set(state.taskList, payload, {
        ...state.taskList[payload],
        editable: true
      });
    },
    [UPDATE_TASK](state, { task, index }) {
      Vue.set(state.taskList, index, {
        ...task,
        editable: false
      });
    }
  },
  actions: {
    [ADD_TASK]({ commit }, task) {
      commit(ADD_TASK, task);
    },
    [DELETE_TASK]({ commit }, taskId) {
      commit(DELETE_TASK, taskId);
    },
    [SET_COMPLETE]({ commit }, index) {
      commit(SET_COMPLETE, index);
    },
    [SET_EDITABLE]({ commit }, index) {
      commit(SET_EDITABLE, index);
    },
    [UPDATE_TASK]({ commit }, data) {
      commit(UPDATE_TASK, data);
    }
  },
  getters: {
    [TASK_LIST_GETTER]: (state) => state.taskList,
  }
});