import Vue from 'vue'
import store from './store';
import App from './App.vue'
import './assets/css/skeleton.css';
import './assets/css/font-awesome.css';

Vue.config.productionTip = false;


new Vue({
  store,
  el: '#app',
  render: h => h(App),
});
